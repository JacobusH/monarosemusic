export interface PagePictureContainer {
  key: string,
  shortID: string,
  imgs: PagePicture[];
  isActive: boolean,
  createdAt: Date,
  updatedAt: Date
}

export interface PagePicture { // can be multiple pictures available for one container
  key: string,
  imgUrl: string,
  isActive: boolean,
  createdAt: Date,
  updatedAt: Date
}
