export interface Note {
  key: string,
  message: string,
  replies: Array<Note>,
  isRead?: boolean,
  isFinished: boolean,
  createdAt: Date,
  updatedAt: Date
}
