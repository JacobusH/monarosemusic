export interface GalleryItem {
  key: string,
  caption: string,
  categories: Array<string>,
  imgUrl: string,
  isActive: boolean,
  createdAt: Date,
  updatedAt: Date
}
