export interface Teacher {
  key: string,
  name: string,
  summary: string ,
  instrument: string,
  imgUrl: string,
  isActive: boolean,
  createdAt: Date,
  updatedAt: Date
}
