export interface Reference {
  key: string,
  person: string,
  location: string,
  message: string,
  isRead: boolean,
  createdAt: Date,
  updatedAt: Date
}
