export interface ProfileLink {
  key: string,
  title?: string,
  link: string,
  categories: string,
  createdAt: Date,
  updatedAt: Date
}
