export interface TextSection {
  key: string,
  shortID: string,
  html: string,
}
