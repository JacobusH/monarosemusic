export interface Signup {
  key: string,
  name: string,
  phone: string,
  email: string,
  message: string,
  isRead: boolean,
  createdAt: Date,
  updatedAt: Date
}
