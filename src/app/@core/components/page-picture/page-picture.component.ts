import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { PagePictureService } from 'app/@core/services/_index';
import { PagePictureContainer } from 'app/@core/models/_index';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-page-picture',
  templateUrl: './page-picture.component.html',
  styleUrls: ['./page-picture.component.scss']
})
export class PagePictureComponent implements OnInit, OnDestroy {
  @Input() shortID: string;
  ps$: Subscription;
  actPic;

  constructor(private pagePicService: PagePictureService) {

  }

  ngOnInit() {
    if(this.shortID) {
      this.ps$ = this.pagePicService.getContByShortID(this.shortID).valueChanges().subscribe(x => {
        let conts = x as PagePictureContainer[];
        if(conts.length > 0) {
          this.pagePicService.getActivePagePic(conts[0].key).valueChanges().subscribe(x => {
            this.actPic = x[0];
          })
        }
      })
    }
  }

  ngOnDestroy() {
    this.ps$.unsubscribe();
  }

  getUrl() {
    return this.actPic.imgUrl;
  }

}
