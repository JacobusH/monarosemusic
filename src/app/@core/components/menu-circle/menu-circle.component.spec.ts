import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuCircleComponent } from './menu-circle.component';

describe('MenuCircleComponent', () => {
  let component: MenuCircleComponent;
  let fixture: ComponentFixture<MenuCircleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuCircleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuCircleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
