import { Component, OnInit, Input } from '@angular/core';
import { ReferencesService } from 'app/@core/services/references.service';
import { Reference } from 'app/@core/models/reference.model';

@Component({
  selector: 'app-reference',
  templateUrl: './reference.component.html',
  styleUrls: ['./reference.component.scss']
})
export class ReferenceComponent implements OnInit {
  @Input() ref: Reference;
  @Input() hideText: boolean = false;

  constructor(private referenceService: ReferencesService) { }

  ngOnInit() {

  }

}
