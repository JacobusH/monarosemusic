import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNumbersComponent } from './page-numbers.component';

describe('PageNumbersComponent', () => {
  let component: PageNumbersComponent;
  let fixture: ComponentFixture<PageNumbersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageNumbersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNumbersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
