import { Component, OnInit, Input, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-page-numbers',
  templateUrl: './page-numbers.component.html',
  styleUrls: ['./page-numbers.component.scss']
})
export class PageNumbersComponent implements OnInit, OnChanges, OnDestroy {
  @Input() items$: Observable<any[]>;
  itemSub: Subscription;

  constructor() {
    console.log('iteem', this.items$)
  }

  ngOnInit() {
    this.itemSub = this.items$.subscribe(x => {
      console.log('bleh', x)
    })
  }

  ngOnChanges(changes: SimpleChanges) {
    // if(changes.items) {
    //   console.log('change', changes.items.currentValue)
    //   // this.items = changes.items.currentValue;
    //   this.items = changes.items;
    // }
  }

  ngOnDestroy() {
    this.itemSub.unsubscribe();
  }


}
