import { Component, OnInit, ViewEncapsulation, OnChanges, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ProfileLink } from 'app/@core/models/_index';
import { ProfileLinksService } from 'app/@core/services/_index';
import { Observable } from 'rxjs/Observable';
import { apparateAnimation } from 'app/@core/animations/_index';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profile-link-list',
  templateUrl: './profile-link-list.component.html',
  styleUrls: ['./profile-link-list.component.scss'],
  animations: [ apparateAnimation ]
})
export class ProfileLinkListComponent implements OnInit, OnDestroy {
  @Input() filterBy?: string = 'all';
  @Input() isAdmin: boolean = false;
  @Output() onItemSelect: EventEmitter<ProfileLink> = new EventEmitter<ProfileLink>();
  pl$: Subscription;
  profileLinks: Observable<ProfileLink[]>;
  filterOptions: Array<string> = [];
  filterOptionsSorted: string[];
  filterMetadata: any;

  currentPage: number = 0;
  totalPages: number = 1;
  pageSize: number = 10;

  constructor(private pflService: ProfileLinksService) { }

  ngOnInit() {
    this.profileLinks = this.pflService.profileLinks.valueChanges();
    this.pl$ = this.profileLinks.subscribe(x => {
      for(var i = 0; i < x.length; i++) {
        let splits = x[i].categories.split(',');
        for(var j = 0; j < splits.length; j++) {
          // var element = splits[j].replace(/\s/g, '');
          var element = splits[j];
          if(this.filterOptions.indexOf(element) === -1) {
            this.filterOptions.push(element.replace(/\n/g,''));
          }
        }
      }
      this.filterOptionsSorted = this.filterOptions.sort((n1,n2) => {
        if (n1 > n2) {return 1;}
        if (n1 < n2) {return -1;}
        return 0;
      });
      this.filterOptionsSorted.unshift('All');
      this.filterBy = "Classical";
    })
  }

  ngOnDestroy() {
    this.pl$.unsubscribe();
  }

  filterClicked(filterApplied: string) {
    this.filterBy = filterApplied;
  }

  selectItem(item: ProfileLink) {
    this.onItemSelect.emit(item);
  }

}
