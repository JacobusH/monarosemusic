import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileLinkListComponent } from './profile-link-list.component';

describe('ProfileLinkListComponent', () => {
  let component: ProfileLinkListComponent;
  let fixture: ComponentFixture<ProfileLinkListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileLinkListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileLinkListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
