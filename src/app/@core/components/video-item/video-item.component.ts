import { Component, OnInit, ViewEncapsulation, Input, HostListener } from '@angular/core';

@Component({
  selector: 'app-video-item',
  templateUrl: './video-item.component.html',
  styleUrls: ['./video-item.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class VideoItemComponent implements OnInit {
  @Input('videoId') videoId:string;
  @Input('title') title:string;
  @Input('caption') caption:string;
  @Input('tileSize') tileSize:string;
  player: YT.Player;
  windowWidth;

  constructor() {
  }

  ngOnInit() {
    this.windowWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    this.windowWidth = event.target.innerWidth;
    // console.log(event.target.innerWidth);
  }

  savePlayer (player) {
    this.player = player;
    // console.log('player instance', player)
  }

  onStateChange(event){
    // console.log('player state', event.data);
  }


}
