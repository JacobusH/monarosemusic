import { Component, OnInit, Input } from '@angular/core';
import { TextSectionService } from 'app/@core/services/text-section.service';
import { TextSection } from 'app/@core/models/text-section.model';

@Component({
  selector: 'text-section',
  templateUrl: './text-section.component.html',
  styleUrls: ['./text-section.component.scss']
})
export class TextSectionComponent implements OnInit {
  @Input() key: string;
  @Input() shortID: string;
  tsInnerHTML: string;

  constructor(private tsService: TextSectionService) { }

  ngOnInit() {
    if(this.key) {
      this.tsService.getByKey(this.key).subscribe(x => {
        if(x.length > 0) {
          let tmp = x[0] as TextSection;
          this.tsInnerHTML = tmp.html;
        }
      })
    }
    else if(this.shortID) {
      this.tsService.getByShortID(this.shortID).subscribe(x => {
        if(x.length > 0) {
          let tmp = x[0] as TextSection;
          this.tsInnerHTML = tmp.html;
        }
      })
    }
  }

}
