import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { TextSectionService } from 'app/@core/services/text-section.service';
import { TextSection } from 'app/@core/models/text-section.model';
import { FrontendManagerService } from 'app/@core/services/frontend-manager.service';
import { NgForm } from '@angular/forms';
import { from, Observable, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-text-section-edit',
  templateUrl: './text-section-edit.component.html',
  styleUrls: ['./text-section-edit.component.scss']
})
export class TextSectionEditComponent implements OnInit, OnDestroy {
  @Input() shortID: string; // 'home' or 'home1'
  @Input() isChangeID: boolean = false;
  ts$: Subscription;
  tsInnerHTML: string;
  textSections: Observable<{}[]>;
  textSectionsStatic: TextSection[];
  model;
  selectedItem: TextSection;
  currentPage: number = 0;
  totalPages: number = 1;
  pageSize: number = 10;
  isStatic: boolean = false;
  filterOptions: Array<string> = [];

  constructor(private tsService: TextSectionService
    , private frontService: FrontendManagerService) { }

  ngOnInit() {
    this.model = this.tsService.createNew('', '');
    if(!this.shortID) {
      let piece1 = window.location.pathname.lastIndexOf('/')+1;
      let piece2 = window.location.pathname.length;
      this.shortID = window.location.pathname.substring(piece1, piece2);
    }

    this.textSectionsStatic = new Array<TextSection>();
    if(this.shortID == "all") {
      this.textSections = this.tsService.getAll();
    }
    else if(this.hasNumber(this.shortID)) { // get just the specific text-section
      this.textSections = this.tsService.getByShortID(this.shortID);
    }
    else { // get all text-sections matching the string
      // OBS
      this.textSections = this.tsService.getAll().pipe(map(stuff => stuff.filter(
        othStuff => {
          // console.log(othStuff)
          let tmp = othStuff as TextSection;
          // console.log('tmp', tmp)
          return tmp.shortID.indexOf(this.shortID) != -1
        }
      )))
      // STATIC
      // this.tsService.getAll().subscribe(vals => {
      //   let tmp = vals as TextSection[];
      //   tmp.forEach(val => {
      //     if(val.shortID.indexOf(this.shortID) != -1) {
      //       this.textSectionsStatic.push(val);
      //       console.log('text', this.textSectionsStatic)
      //     }
      //   })
      // })
      // this.textSections = from(this.textSectionsStatic);
    }

    this.ts$ = this.textSections.subscribe(x => {
      for(var i = 0; i < x.length; i++) {
        let xx = x[i] as TextSection;
        let splits = xx.shortID.split(',');
        for(var j = 0; j < splits.length; j++) {
          // var element = splits[j].replace(/\s/g, '');
          var element = splits[j];
          if(this.filterOptions.indexOf(element) === -1) {
            this.textSectionsStatic.push(xx);
            this.filterOptions.push(element);
          }
        }
      }
      if(this.filterOptions.length > 0) {
        let xx = x[0] as TextSection;
        this.setSelectedItem(xx);
      }
    })

  }

  ngOnDestroy() {
    this.ts$.unsubscribe();
  }

  filterClicked(filterApplied: string) {
    this.textSectionsStatic.forEach(x => {
      if(x.shortID == filterApplied) {
        this.setSelectedItem(x);
      }
    })
  }

  hasNumber(myString: string) {
    return /\d/.test(myString);
  }

  saveItem(form: NgForm) {
    // editing item
    if(this.selectedItem != null) {
      this.tsService.edit(this.selectedItem).catch(err => console.log(err))
      // form.reset();
    }
    else { // new item
      this.model.key = 'tmp';
      this.tsService.saveNewTextSection(this.model);
      this.model = this.tsService.createNew('', '');
      // form.reset();
    }
  }

  setNewItem() {
    this.selectedItem = null;
    this.model = this.tsService.createNew('', '');
  }

  setSelectedItem(item: TextSection) {
    this.selectedItem = item;
    this.model = item;
  }

}
