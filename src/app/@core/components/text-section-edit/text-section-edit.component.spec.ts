import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextSectionEditComponent } from './text-section-edit.component';

describe('TextSectionEditComponent', () => {
  let component: TextSectionEditComponent;
  let fixture: ComponentFixture<TextSectionEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextSectionEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextSectionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
