import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagePictureEditComponent } from './page-picture-edit.component';

describe('PagePictureEditComponent', () => {
  let component: PagePictureEditComponent;
  let fixture: ComponentFixture<PagePictureEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagePictureEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagePictureEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
