import { Component, OnInit, OnChanges, ViewEncapsulation, ViewChild, Input, SimpleChanges, OnDestroy } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { PagePicture, Upload, PagePictureContainer } from 'app/@core/models/_index';
import { PagePictureService, UploadService } from 'app/@core/services/_index';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { filter, map } from 'rxjs/operators';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-page-picture-edit',
  templateUrl: './page-picture-edit.component.html',
  styleUrls: ['./page-picture-edit.component.scss']
})
export class PagePictureEditComponent implements OnInit, OnDestroy {
  modelPagePic = this.pagePicService.createNewPagePicture();
  modelPagePicCont = this.pagePicService.createNewContainer();
  @Input() test: string;
  @Input() containerKey: string;
  @Input() isChangeID: boolean = false;
  @ViewChild('fileUpload', {static: false}) fileUploadVar: any;
  @ViewChild('imgItemSelected', {static: false}) imgItemSelected: any;
  ps$: Subscription;
  selectedItem: PagePicture;
  selectedFiles: FileList;
  selectedPicture: string;
  currentUpload: Upload;
  conts: Observable<PagePictureContainer[]>;
  items$: Subscription;
  items: Observable<PagePicture[]>;
  // firebaseUrl: string = 'gs://monarosemusic-43b48.appspot.com/'; // mine
  firebaseUrl: string = 'gs://monarosestudios-db5fe.appspot.com/';
  STORAGE_PAGEPICS: string = 'pagePics';
  currentPageConts: number = 0;
  currentPagePics: number = 0;
  totalPages: number = 1;
  pageSize: number = 10;
  filterOptions: Array<{'disp': string, 'key': string}> = [];
  filterBy: string = 'all';

  constructor(private pagePicService: PagePictureService, private upsvc: UploadService) {

  }

  ngOnInit() {
    this.conts = this.pagePicService.pagePicConts.valueChanges();
    this.ps$ = this.pagePicService.pagePicConts.valueChanges().subscribe(x => {
      for(var i = 0; i < x.length; i++) {
        let splits = x[i].shortID.split(',');
        for(var j = 0; j < splits.length; j++) {
          // var element = splits[j].replace(/\s/g, '');
          var element = splits[j];
          // if(this.filterOptions.indexOf(element) === -1) {
          //   this.filterOptions.push(element);
          // }
          let isFound = false;
          this.filterOptions.forEach(opt => {
            if(opt.disp == element) {
              isFound = true;
            }
          })
          if(!isFound) {
            this.filterOptions.push({'disp': element, 'key': x[i].key});
          }
        }
      }
      if(this.filterOptions.length > 0) {
        this.items = this.pagePicService.getPagePics(this.filterOptions[0].key).valueChanges();
      }
    })
  }

  ngOnChanges(changes: SimpleChanges) {
    // on first selection
    if(changes.containerKey.previousValue == null && changes.containerKey.currentValue != null)  {
      this.getNewItems();
    }
    // on future changes
    if(changes.containerKey.previousValue != null && changes.containerKey.currentValue != null) {
      this.items$.unsubscribe();
      this.getNewItems();
    }
  }

  ngOnDestroy() {
    this.ps$.unsubscribe();
    this.items$.unsubscribe();
  }

  filterClicked(filterApplied: string) {
    this.filterBy = filterApplied;
    this.setPagePics(filterApplied);
  }

  getNewItems() {
    this.items = this.pagePicService.getPagePics(this.containerKey).valueChanges();
    this.items$ = this.items.subscribe();
  }

  setPagePics(contKey: string) {
    this.containerKey = contKey;
    this.getNewItems();
  }

  saveItem() {
    // uploading new pic
    if(this.selectedFiles != null) {
      this.uploadSingleItem();
      this.modelPagePic = this.pagePicService.createNewPagePicture();
      this.fileUploadVar.nativeElement.value = "";
      // form.reset();
    }
    // editing pic
    else if(this.selectedItem != null) {
      this.pagePicService.editPagePic(this.containerKey, this.selectedItem)
      this.fileUploadVar.nativeElement.value = "";
      // form.reset();
    }
  }

  saveNewID(form: NgForm) {
    // editing item
    if(this.selectedItem != null) {
      // this.pagePicService.edit(this.selectedItem).catch(err => console.log(err))
      // form.reset();
    }
    else { // new item
      this.modelPagePicCont.key = 'tmp';
      this.pagePicService.savePagePictureCont(this.modelPagePicCont);
      // this.model = this.tsService.createNew('', '');
      form.reset();
    }
  }

  detectFiles(event) {
    this.selectedFiles = event.target.files;
  }

  uploadSingleItem() {
    let file = this.selectedFiles.item(0);
    this.currentUpload = new Upload(file);

    this.upsvc.pushUpload(this.currentUpload, this.STORAGE_PAGEPICS, this.modelPagePic, this.containerKey);
  }

  setNewItem() {
    this.selectedItem = null;
    this.modelPagePic = this.pagePicService.createNewPagePicture();
    this.imgItemSelected.nativeElement.src = "";
  }

  setSelectedItem(item: PagePicture) {
    let pics$ = this.pagePicService.getPagePics(this.containerKey).valueChanges().subscribe(x => {
      if(x) {
        let pics = x as PagePicture[];
        pics.forEach(pic => {
          if(pic.isActive) {
            pic.isActive = false;
            this.pagePicService.editPagePic(this.containerKey, pic);
          }
        })
      }
      pics$.unsubscribe();
    })

    // this.pagePicService.getActivePagePics(this.containerKey).valueChanges().subscribe(x => {
    //   console.log('shit', x)
    //   if(x) {
    //     let pics = x as PagePicture[];
    //     pics.forEach(pic => {
    //       if(pic.isActive) {
    //         pic.isActive = false;
    //         this.pagePicService.editPagePic(this.containerKey, pic);
    //       }
    //     })
    //   }
    // }).unsubscribe()
    // now set new selection for view
    this.selectedItem = item;
    this.modelPagePic = item;
    // and save new one as active
    item.isActive = true;
    this.pagePicService.editPagePic(this.containerKey, item);

    // let itemRef = this.storageRef.child(item.imgUrl);
    // itemRef.getDownloadURL().then((url) => {
    //   this.selectedPicture = url;
    // })
    // .catch((err) => {
    //   console.log(err);
    // });

  }

  deleteItem() {
    this.pagePicService.deletePagePic(this.containerKey, this.selectedItem);
  }


}
