import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { PagePictureService } from 'app/@core/services/page-picture.service';
import { PagePictureContainer } from 'app/@core/models/_index';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-page-picture-cont',
  templateUrl: './page-picture-cont.component.html',
  styleUrls: ['./page-picture-cont.component.scss']
})
export class PagePictureContComponent implements OnInit, OnDestroy {
  @Input() shortID: string;
  ps$: Subscription;
  pagePics;
  actPic;

  constructor(private pagePicService: PagePictureService) {

  }

  ngOnInit() {
    this.ps$ = this.pagePicService.getContByShortID(this.shortID).valueChanges().subscribe(x => {
      let conts = x as PagePictureContainer[];
      if(conts.length > 0) {
        this.pagePicService.getActivePagePic(conts[0].key).valueChanges().subscribe(x => {
          this.actPic = x[0];
        })
      }
    })
  }

  ngOnDestroy() {
    this.ps$.unsubscribe();
  }

}
