import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagePictureContComponent } from './page-picture-cont.component';

describe('PagePictureContComponent', () => {
  let component: PagePictureContComponent;
  let fixture: ComponentFixture<PagePictureContComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagePictureContComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagePictureContComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
