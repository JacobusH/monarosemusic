import { Component, OnInit, ViewEncapsulation, OnChanges, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { VideoItem, Upload } from 'app/@core/models/_index';
import { VideoItemService } from 'app/@core/services/_index';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent implements OnInit, OnDestroy {
  @Input() filterBy?: string = 'all';
  @Output() onItemSelect: EventEmitter<VideoItem> = new EventEmitter<VideoItem>();
  va$: Subscription;
  videosActive: Observable<VideoItem[]>;
  filterOptions: Array<string> = [];
  filterMetadata: any;

  currentPage: number = 0;
  totalPages: number = 1;
  pageSize: number = 5;

  constructor(private videoService: VideoItemService) {
    // this.videosActive = this.videoService.videoItemsActive.valueChanges();
  }

  ngOnInit() {
    this.videosActive = this.videoService.videoItemsActive.valueChanges();
    this.va$ = this.videosActive.subscribe(x => {
      for(var i = 0; i < x.length; i++) {
        let splits = x[i].categories.split(',');
        for(var j = 0; j < splits.length; j++) {
          var element = splits[j].replace(/\s/g, '');
          if(this.filterOptions.indexOf(element) === -1) {
            this.filterOptions.push(element);
          }
        }
      }
    })
  }

  ngOnChanges() {

  }

  ngOnDestroy(): void {
    this.va$.unsubscribe();
  }

  filterClicked(filterApplied: string) {
    this.filterBy = filterApplied;
    this.currentPage = 0;
  }

  selectItem(item: VideoItem) {
    this.onItemSelect.emit(item);
  }

}
