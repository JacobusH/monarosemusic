import { ModuleWithProviders, NgModule, Optional, SkipSelf, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
// import { NbAuthModule, NbDummyAuthStrategy } from '@nebular/auth';
// import { NbSecurityModule, NbRoleProvider } from '@nebular/security';
// import {
//   NbActionsModule,
//   NbLayoutModule,
//   NbMenuModule,
//   NbSearchModule,
//   NbSidebarModule,
//   NbUserModule,
//   NbContextMenuModule,
//   NbButtonModule,
//   NbSelectModule,
//   NbIconModule,
//   NbThemeModule,
// } from '@nebular/theme';
import { NgxYoutubePlayerModule } from 'ngx-youtube-player';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { NbEvaIconsModule } from '@nebular/eva-icons';
import { of as observableOf } from 'rxjs';
// import { DEFAULT_THEME } from 'app/styles/theme.default';
// import { DARK_THEME } from 'app/styles/theme.dark';

import { throwIfAlreadyLoaded } from './module-import-guard';
import { AnalyticsService } from './utils';

import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AlertComponent } from './components/alert/alert.component';
// import { LayoutDirectionSwitcherComponent, SearchInputComponent, SwitcherComponent } from './components';
import { TextSectionComponent } from './components/text-section/text-section.component';
import { TextSectionEditComponent } from './components/text-section-edit/text-section-edit.component';
import { VideoItemComponent } from './components/video-item/video-item.component';

import { TimesPipe, CapitalizePipe, NumberWithCommasPipe, PluralPipe, TimingPipe, RoundPipe, ImageFilterPipe, SafePipe } from './pipes/_index';
import { VideoListComponent } from './components/video-list/video-list.component';
import { PagePictureComponent } from './components/page-picture/page-picture.component';
import { PagePictureEditComponent } from './components/page-picture-edit/page-picture-edit.component';
import { ImageGalleryComponent } from './components/image-gallery/image-gallery.component';
import { PagePictureContComponent } from './components/page-picture-cont/page-picture-cont.component';
import { PageNumbersComponent } from './components/page-numbers/page-numbers.component';
import { ReferenceComponent } from './components/reference/reference.component';
import { ProfileLinkListComponent } from './components/profile-link-list/profile-link-list.component';
import { MenuCircleComponent } from './components/menu-circle/menu-circle.component';

const PIPES = [
  TimesPipe
  , CapitalizePipe
  , NumberWithCommasPipe
  , PluralPipe
  , TimingPipe
  , RoundPipe
  , ImageFilterPipe
  , SafePipe
];

const COMPONENTS = [
  AlertComponent,
  ImageGalleryComponent,
  MenuCircleComponent,
  PageNumbersComponent,
  PagePictureComponent,
  PagePictureContComponent,
  PagePictureEditComponent,
  ProfileLinkListComponent,
  ReferenceComponent,
  TextSectionComponent,
  TextSectionEditComponent,
  VideoListComponent,
  VideoItemComponent
];

// const NBMODULES = [
//   NbIconModule,
//   NbSelectModule,
//   NbLayoutModule,
//   NbThemeModule,
//   NbSidebarModule,
//   NbButtonModule,
//   NbContextMenuModule,
//   NbUserModule,
//   NbSearchModule,
//   NbMenuModule,
//   NbActionsModule
// ];

@NgModule({
  imports: [
    CommonModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFontAwesomeModule,
    // NbActionsModule,
    // NbIconModule,
    // NbSearchModule,
    // NbActionsModule,
    // NbLayoutModule,
    // NbSidebarModule,
    // NbMenuModule,
    FormsModule,
    ReactiveFormsModule,
    NgxYoutubePlayerModule
  ],
  exports: [
    // NbAuthModule,
    // NbActionsModule,
    // NbIconModule,
    // NbSearchModule,
    // NbActionsModule,
    // NbLayoutModule,
    // NbSidebarModule,
    // NbMenuModule,
    AngularFontAwesomeModule,
    ... PIPES,
    ... COMPONENTS
  ],
  declarations: [
    ... COMPONENTS,
    ... PIPES
  ],
  providers: [
    AnalyticsService,
    // NbThemeModule.forRoot(
    //   {
    //     name: 'dark',
    //   },
    //   [ DARK_THEME ],
    // ).providers,
  ]
})
export class CoreModule {
  // constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
  //   throwIfAlreadyLoaded(parentModule, 'CoreModule');
  // }

  // static forRoot(): ModuleWithProviders {
  //   return <ModuleWithProviders>{
  //     ngModule: CoreModule,
  //     providers: [
  //       ...NB_CORE_PROVIDERS,
  //     ],
  //   };
  // }
}
