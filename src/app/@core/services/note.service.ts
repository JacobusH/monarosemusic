import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs/Observable';
import { Note } from 'app/@core/models/_index';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class NoteService {
  COL = 'notes';
  notes: AngularFirestoreCollection<Note>;
  notesFinished: AngularFirestoreCollection<Note>;

  constructor(private afs: AngularFirestore) {
    this.notes = this.afs.collection(this.COL, ref => ref.orderBy('createdAt', 'desc'));
    this.notesFinished = this.afs.collection(this.COL, ref => ref.where('isFinished', '==', 'true').orderBy('createdAt', 'desc'));
  }

  createNew(): Note {
    let ts: Note = {
      key: '',
      message: '',
      replies: [],
      isFinished: false,
      createdAt: new Date(),
      updatedAt: new Date()
    };
    return ts;
  }

  saveNewNote(c: Note) {
    let promise: Promise<firebase.firestore.DocumentReference> = this.notes.add(c);
    promise.then(x => {
      x.update({key: x.id});
    });

    return promise;
  }

  edit(item: Note): Promise<void> {
    return this.notes.doc(item.key).update(item);
  }

  delete(item: Note): Promise<void> {
    return this.notes.doc(item.key).delete();
  }

}
