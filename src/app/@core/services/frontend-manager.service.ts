import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { PagePicture, Upload, PagePictureContainer } from 'app/@core/models/_index';

@Injectable({
  providedIn: 'root'
})
export class FrontendManagerService {
  private subject = new Subject<string>();

  constructor() { }

  getFrontendID(): Observable<any> {
    return this.subject.asObservable();
  }

  setFrontendID(cont: PagePictureContainer) {
    this.subject.next(cont.shortID);
  }

}
