import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class VersionService {
  appVersion = 2;
  isCreateNewSections = false;
  private dataSource = new BehaviorSubject<any>(this.appVersion);
  appVersion$ = this.dataSource.asObservable();
  private dataSource2 = new BehaviorSubject<any>(this.isCreateNewSections);
  isCreateNewSections$ = this.dataSource2.asObservable();

  constructor() {
    this.setAppVersion(this.appVersion);
  }

  getAppVersion(): Observable<any> {
    return this.appVersion$;
  }

  getIsCreate(): Observable<any> {
    return this.isCreateNewSections$;
  }

  setAppVersion(version: number) {
    // console.log('in set', version)
    this.dataSource.next(version);
  }

  setIsCreate(v) {
    // console.log('in set', v)
    this.dataSource2.next(v);
  }
}
