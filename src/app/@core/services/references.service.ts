import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs/Observable';
import { Reference } from 'app/@core/models/reference.model';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class ReferencesService {
  COL = 'references';
  references: AngularFirestoreCollection<Reference>;

  constructor(private afs: AngularFirestore) {
    this.references = this.afs.collection(this.COL, ref => ref.orderBy('createdAt'));
  }

  createNew(): Reference {
    let ts: Reference = {
      key: '',
      person: '',
      location: '',
      message: '',
      isRead: false,
      createdAt: new Date(),
      updatedAt: new Date()
    };
    return ts;
  }

  save(c: Reference) {
    let promise: Promise<firebase.firestore.DocumentReference> = this.references.add(c);
    promise.then(x => {
      x.update({key: x.id});
    });

    return promise;
  }

  edit(item: Reference): Promise<void> {
    return this.references.doc(item.key).update(item);
  }

  delete(item: Reference): Promise<void> {
    return this.references.doc(item.key).delete();
  }

}
