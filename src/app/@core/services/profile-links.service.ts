import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs/Observable';
import { ProfileLink } from 'app/@core/models/_index';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class ProfileLinksService {
  COL = 'profileLinks';
  profileLinks: AngularFirestoreCollection<ProfileLink>;

  constructor(private afs: AngularFirestore) {
    this.profileLinks = this.afs.collection(this.COL, ref => ref.orderBy('createdAt'));
  }

  createNew(): ProfileLink {
    let ts: ProfileLink = {
      key: '',
      title: '',
      link: '',
      categories: '',
      createdAt: new Date(),
      updatedAt: new Date()
    };
    return ts;
  }

  getByCategory(cat: string) {
    return this.afs.collection(this.COL , ref => ref.where('categories', '==', cat));
  }

  save(c: ProfileLink) {
    let promise: Promise<firebase.firestore.DocumentReference> = this.profileLinks.add(c);
    promise.then(x => {
      x.update({key: x.id});
    });

    return promise;
  }

  edit(item: ProfileLink): Promise<void> {
    return this.profileLinks.doc(item.key).update(item);
  }

  delete(item: ProfileLink): Promise<void> {
    return this.profileLinks.doc(item.key).delete();
  }

}
