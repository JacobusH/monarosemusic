import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { Upload, PagePicture, GalleryItem } from 'app/@core/models/_index';
import { GalleryService, TeacherService } from 'app/@core/services/_index';
import { PagePictureService } from 'app/@core/services/page-picture.service';
import "firebase/storage";

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  // basePathGallery:string = '/gallery';
  // basePathTeachers:string = '/teachers';

  storage = firebase.storage();
  storageRef = this.storage.ref();


  constructor(public pagePicService: PagePictureService
    , private galleryService: GalleryService
    , private teacherService: TeacherService) {

  }

  pushUpload(upload: Upload, location: string, model: any, contKey?: string) {
    let storageRef = firebase.storage().ref();
    let uploadTask = storageRef.child(`${location}/${upload.file.name}`).put(upload.file);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, {
      next : (snapshot) => {
      // upload in progress
      upload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
    }, error: (error) => {
      // upload failed
      console.log(error)
    }, complete: () => {
      // upload success
      upload.url = uploadTask.snapshot.downloadURL;
      upload.name = upload.file.name;
      // this.pagePicService.savePagePicture(contKey, model, upload);

      let fileName = upload.name;
      switch(location) {
        case 'pagePics':
          this.pagePicService.savePagePicture(contKey, model, upload);
          break;
        case 'teachers':
          this.teacherService.save(model, upload);
      }


    }});
  }


  deleteUpload(upload: Upload) {
    this.deleteFileStorage(upload.name)
  }

  // Firebase files must have unique names in their respective storage dir
  // So the name serves as a unique key
  deleteFileStorage(name:string) {
    // let storageRef = firebase.storage().ref();
    // storageRef.child(`${this.basePathGallery}/${name}`).delete()
  }

}
