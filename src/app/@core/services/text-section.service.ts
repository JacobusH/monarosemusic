import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs/Observable';
import { TextSection } from 'app/@core/models/text-section.model';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/switchMap'
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class TextSectionService {
  COL = 'textSections';
  textSections: AngularFirestoreCollection<TextSection>;

  constructor(private afs: AngularFirestore) {
    this.textSections = this.afs.collection(this.COL);
  }

  createNew(html: string, shortID: string): TextSection {
    let ts: TextSection = {
      key: '',
      shortID: shortID,
      html: html
    };
    return ts;
  }

  saveNewTextSection(t: TextSection) {
    let promise: Promise<firebase.firestore.DocumentReference> = this.textSections.add(t);
    promise.then(x => {
      x.update({key: x.id});
    });

    return promise;
  }

  edit(item: TextSection): Promise<void> {
    return this.textSections.doc(item.key).update(item);
  }

  getByKey(key: string) {
    return this.afs.collection(`${this.COL}/${key}`).valueChanges();
  }

  getByShortID(shortID: string) {
    return this.afs.collection(this.COL, ref => ref.where('shortID', '==', shortID)).valueChanges();
  }

  getAll(): Observable<any> {
    return this.afs.collection(`${this.COL}`, ref => ref.orderBy('shortID', 'asc')).valueChanges();
  }

  delete(item: TextSection): Promise<void> {
    return this.textSections.doc(item.key).delete();
  }

}
