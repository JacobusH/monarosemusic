import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs/Observable';
import { ContactMsg } from 'app/@core/models/contact.model';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  COL = 'contactMsgs';
  contactMsgs: AngularFirestoreCollection<ContactMsg>;

  constructor(private afs: AngularFirestore) {
    this.contactMsgs = this.afs.collection(this.COL, ref => ref.orderBy('createdAt', 'desc'));
  }

  createNew(): ContactMsg {
    let ts: ContactMsg = {
      key: '',
      email: '',
      name: '',
      phone: '',
      message: '',
      isRead: false,
      createdAt: new Date(),
      updatedAt: new Date()
    };
    return ts;
  }

  saveNewContact(c: ContactMsg) {
    let promise: Promise<firebase.firestore.DocumentReference> = this.contactMsgs.add(c);
    promise.then(x => {
      x.update({key: x.id});
    });

    return promise;
  }

  edit(item: ContactMsg): Promise<void> {
    return this.contactMsgs.doc(item.key).update(item);
  }

  delete(item: ContactMsg): Promise<void> {
    return this.contactMsgs.doc(item.key).delete();
  }

}
