import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs/Observable';
import { Signup } from 'app/@core/models/signup.model';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class SignupService {
  COL = 'signups';
  signups: AngularFirestoreCollection<Signup>;

  constructor(private afs: AngularFirestore) {
    this.signups = this.afs.collection(this.COL, ref => ref.orderBy('createdAt'));
  }

  createNew(): Signup {
    let ts: Signup = {
      key: '',
      email: '',
      name: '',
      phone: '',
      message: '',
      isRead: false,
      createdAt: new Date(),
      updatedAt: new Date()
    };
    return ts;
  }

  saveNewSignup(c: Signup) {
    let promise: Promise<firebase.firestore.DocumentReference> = this.signups.add(c);
    promise.then(x => {
      x.update({key: x.id});
    });

    return promise;
  }

  edit(item: Signup): Promise<void> {
    return this.signups.doc(item.key).update(item);
  }

  delete(item: Signup): Promise<void> {
    return this.signups.doc(item.key).delete();
  }

}
