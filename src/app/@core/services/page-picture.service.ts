import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PagePicture, PagePictureContainer, Upload } from 'app/@core/models/_index';
import 'rxjs/add/operator/switchMap'
import * as firebase from 'firebase/app';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PagePictureService {
  pagePicConts: AngularFirestoreCollection<PagePictureContainer>;
  pagePicContsActive: AngularFirestoreCollection<PagePictureContainer>;
  storage = firebase.storage();
  storageRef = this.storage.ref();
  COL_CONTS = 'pagePicConts';
  COL_PICS = 'pagePics';
  LOC = 'pagePics';

  constructor(private afs: AngularFirestore) {
    this.pagePicConts = this.afs.collection(this.COL_CONTS, ref => ref.orderBy('shortID'));
    this.pagePicContsActive = this.afs.collection(this.COL_CONTS, ref => ref.where('isActive', '==', true));
  }

  createNewContainer(shortID = ''): PagePictureContainer {
    let data: PagePictureContainer = {
      key: '',
      shortID: shortID,
      imgs: new Array<PagePicture>(),
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date()
    };
    return data;
  }

  createNewPagePicture(): PagePicture {
    let data: PagePicture = {
      key: '',
      imgUrl: 'tmp',
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date()
    };
    return data;
  }

  // CONTAINERS
  savePagePictureCont(t: PagePictureContainer): Promise<firebase.firestore.DocumentReference>  {
    let promise: Promise<firebase.firestore.DocumentReference> = this.pagePicConts.add(t);
    promise.then(x => {
      x.update({key: x.id});
    });

    return promise;
  }

  getAllConts(): Observable<any> {
    return this.afs.collection(this.COL_CONTS).valueChanges();
  }

  getContByShortID(shortID: string) {
    return this.afs.collection(this.COL_CONTS, ref => ref.where('shortID', '==', shortID));
  }

  editCont(item: PagePictureContainer): Promise<void> {
    return this.pagePicConts.doc(item.key).update(item);
  }

  deleteCont(item: PagePictureContainer): Promise<void> {
    return this.pagePicConts.doc(item.key).delete();
  }

  // PAGE PICTURES
  savePagePicture(contKey: string, item: PagePicture, up: Upload) {
    this.pagePicConts.doc(contKey).collection(this.COL_PICS).add(item).then(x => {
      x.update({key: x.id});

      let itemRef = this.storageRef.child(this.LOC +'/' + up.name);
      itemRef.getDownloadURL().then((url) => {
        this.pagePicConts.doc(contKey).collection(this.COL_PICS).doc(x.id).update({imgUrl: url});
      })
      .catch((err) => {
        console.log(err);
      });
    })
  }

  getPagePics(contKey: string): AngularFirestoreCollection<PagePicture> {
    return this.pagePicConts.doc(contKey).collection(this.COL_PICS);
  }

  getActivePagePic(contKey: string): AngularFirestoreCollection<PagePicture> {
    return this.pagePicConts.doc(contKey).collection(this.COL_PICS, ref => ref.where('isActive', '==', true));
  }

  getActivePagePicByShortID(shortID: string) {
    this.afs.collection(this.COL_CONTS, ref => ref.where('shortID', '==', true));
    // return this.pagePicConts.doc(contKey).collection(this.COL_PICS, ref => ref.where('isActive', '==', true));
  }

  editPagePic(contKey: string, pic: PagePicture) {
    return this.pagePicConts.doc(contKey).collection(this.COL_PICS).doc(pic.key).update(pic).catch(err => console.log('pic err', err));
  }

   deletePagePic(contKey: string, pic: PagePicture): Promise<void> {
    return this.pagePicConts.doc(contKey).collection(this.COL_PICS).doc(pic.key).delete();
  }


  // getByShortID(contKey: string, shortID: string) {
  //   return this.afs.collection(this.COL, ref => ref.where('shortID', '==', shortID)).valueChanges();
  // }

  // edit(contkey: string, item: PagePicture): Promise<void> {
  //   return this.pagePics.doc(item.key).update(item);
  // }

  // updateURL(contkey: string, item: PagePicture, url: string): Promise<void> {
  //   //  this.afs.doc('teachers/' + item.key).update({imgUrl: url});
  //   return this.pagePics.ref.doc(item.key).update({imgUrl: url});
  // }



}
