import { animate, state, style, transition, trigger, query, stagger, group } from '@angular/animations';

export const dashAnimation = trigger('dashAnimation', [
  transition('0 => 1', [
    group([
      style({ height: '5%'}),
      animate('0.75s', style({ height: '100%'})),
      // query('.items',
      //   [style({opacity: 0, transform: 'translateX(100px)' }), stagger('60ms', animate('600ms ease-out', style({ opacity: 1, transform: 'none' })))], // list
      //   { optional: true }
      // )
      // query('.items', style({ opacity: 0 })),
      // query('.items', animate(1000, style({ opacity: 1 })))
    ])
  ]),
  transition('1 => 0', [
    // animate('0.75s', style({ width: 'auto', height: 'auto'})),
    // style({opacity: 1}), animate('0.5s', style({ opacity: 0})), //  parent
    // query(':leave',
    //   [ stagger('-60ms', animate('600ms ease-out', style({ opacity: 0})))], // list
    //   { optional: true }
    // ),
    // style({opacity: 1}), animate('0.5s', style({ opacity: 0})), //  parent
    style({ height: '100%'}),
    animate('0.75s', style({ height: '5%'})), //  parent
  ])
]);
