import { trigger, state, animate, transition, style } from '@angular/animations';

export const slideUpDownAnimation =
trigger('slideUpDownAnimation', [
  state('true', style({
    transform: 'translateY(0)'
  })),
  state('false', style({
    transform: 'translateY(auto)'
  })),
  transition('down <=> up', animate(300))
]);

