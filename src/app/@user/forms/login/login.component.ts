import { Component, OnInit } from '@angular/core';
import { AuthService } from 'app/@core/services/auth.service';
import { AlertService } from 'app/@core/services/alert.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: {
    'email': string,
    'password': string,
    'fullName': string
  };

  showMessages = {
    'success': true,
    'error': true
  };
  cc = {'success': true};
  rememberMe = true;
  submitted = false;
  socialLinks

  constructor(
    public authService: AuthService
    , private alertService: AlertService) {
    this.user = {
      email: '',
      password: '',
      fullName: ''
    };
  }

  ngOnInit() {
    // this.authService.errors$.subscribe(x => {
    //   console.log("login errros", x)
    // })
  }

  login() {
    this.authService.emailLogin(this.user.email, this.user.password);
  }

}
