import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { NgForm } from '@angular/forms';
import { AuthService } from 'app/@core/services/auth.service';
import { User, Roles } from 'app/@core/models/user.model';
import { UserService } from 'app/@core/services/user.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  user: User;
  tmpUser: {'password': null, 'confirmPassword': null}; // for passing around email password only
  showMessages = {
    'success': true,
    'error': true
  };
  cc = {'success': true};
  submitted = false;

  constructor(
    private authService: AuthService
    , private userService: UserService
    , private _location: Location
  ) {
    this.user = this.userService.createNew();
    this.tmpUser = {'password': null, 'confirmPassword': null};
  }

  ngOnInit() {

  }

  register() {
    this.authService.emailSignup(this.user, this.tmpUser.password);
  }

  registerFacebook() {
    // TODO: register app with facebook
    // this.authService.
  }

  registerGoogle() {
    this.authService.googleLogin();
  }

  back() {
    this._location.back();
  }

}
