import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router, NavigationStart }  from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';

@Component({
  selector: 'forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit, OnDestroy {
  router$: Subscription;
  isRegister: boolean;

  constructor(private actRoute: ActivatedRoute
    , private router: Router
    , private loc: Location) {
      if(window.location.pathname.indexOf('register') != -1) {
        this.isRegister = true;
      }
      else if(window.location.pathname.indexOf('login') != -1) {
        this.isRegister = false;
      }
  }

  ngOnInit() {
    this.router$ = this.router.events.subscribe(event => {
      if (event.constructor.name === 'NavigationStart') {
        let ev = event as any;
        if(ev.url.indexOf('register') != -1) {
          this.isRegister = true;
        }
        else if(ev.url.indexOf('login') != -1) {
          this.isRegister = false;
        }
      }
    });
  }

  ngOnDestroy() {
    this.router$.unsubscribe();
  }

  back() {
    this.loc.back();
  }


}
