import { Component, OnInit, Input, OnChanges, SimpleChanges} from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from 'app/@core/services/auth.service';
import { SidebarService } from 'app/@core/services/sidebar.service';
import { first, tap } from 'rxjs/operators';
import { User } from 'app/@core/models/user.model';
import { UserService } from 'app/@core/services/user.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header-user',
  templateUrl: './header-user.component.html',
  styleUrls: ['./header-user.component.scss']
})
export class HeaderUserComponent implements OnInit, OnChanges {
  @Input() user$: Observable<User>;
  @Input() title: string;
  dispName: string = "";

  constructor(private authService: AuthService
    , private sidebarService: SidebarService) { }

  ngOnInit() {
    // this.authService.user$.subscribe(x => {
    //   console.log('userheady', x)
    // })

    // this.user$.subscribe(x => {
    //   console.log('heady', x)
    // })

    if(this.title.toLocaleLowerCase() === "profile") {
      this.title = "HOME";
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if(this.title.toLocaleLowerCase() === "profile") {
      this.title = "HOME";
    }
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle();
    return false;
  }

}
