import { Component, OnInit, AfterViewInit, HostListener, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router}  from '@angular/router';
import { AuthService } from 'app/@core/services/auth.service';
import { UserService } from 'app/@core/services/user.service';
import { SidebarService } from 'app/@core/services/sidebar.service';
import { Observable, Subscription, Subscribable } from 'rxjs';
import { User } from 'app/@core/models/user.model';
import { AngularFireAuth } from '@angular/fire/auth';
import  { first, tap } from 'rxjs/operators';
import { ThrowStmt } from '@angular/compiler';
import { MENU_ITEMS } from './profile.menu';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  r$: Subscription;
  a$: Subscription;
  isLoggedIn = false;
  user: User;
  title: string;
  isSidebarLarge: boolean;
  isSidebarMedium: boolean;
  isSidebarNone: boolean;
  isSidebarToggleShow: boolean;
  windowWidth: number;
  menu = MENU_ITEMS;

  constructor(
    public auth: AuthService
    , private afAuth: AngularFireAuth
    , private userService: UserService
    , private router: Router
    , private sidebarService: SidebarService
    ) {
      this.r$ = this.router.events.subscribe(x => {
        let adminIdx = window.location.pathname.indexOf('user');
        if(adminIdx != -1) {
          // this.title = window.location.pathname.substring(adminIdx, window.location.pathname.length)
          this.title = window.location.pathname.substring(window.location.pathname.lastIndexOf('/')+1, window.location.pathname.length).toLocaleUpperCase();
        }
      })
    }

  ngOnInit() {
    this.a$ = this.auth.user$.subscribe(x => {
      // console.log("profiley", x)
      this.user = x;
      if(x.roles.guest == true) { // remove other options if guest
        for(let i = this.menu.length-1; i >= 0; i--) {
          if(this.menu[i].title != "Logout" && this.menu[i].title != "Profile" && this.menu[i].title != "Front") {
            this.menu.splice(i, 1);
          }
        }
      }
      if(x.roles.admin == true) {
        this.menu.push({'title': 'Admin', 'link': '/admin', 'icon': 'shield-star'})
      }
    });

    this.windowWidth = window.innerWidth;
    this.sidebarService.isSidebarToggleShow = true;
    this.getSidebarSize(this.windowWidth);
    if(this.isSidebarNone) {
      this.sidebarService.isSidebarToggleShow = false;
    }
  }

  ngOnDestroy() {
    this.r$.unsubscribe();
    this.a$.unsubscribe();
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    this.windowWidth = event.target.innerWidth;
    this.getSidebarSize(this.windowWidth);
    // console.log(event.target.innerWidth);
  }

  userLoggedIn() {
      return this.afAuth.authState.pipe(first());
  }

  signOut() {
    this.auth.signOut();
  }

  getSidebarToggleStatus() {
    if(!this.isSidebarNone && this.sidebarService.isSidebarToggleShow) {
      return true;
    }
    if(this.isSidebarNone && !this.sidebarService.isSidebarToggleShow) {
      return false;
    }
    else if(!this.sidebarService.isSidebarToggleShow) {
      return false;
    }
    else {
      return true;
    }
  }

  getSidebarSize(ww) {
    if(ww > 666) {
      this.isSidebarLarge = true;
      this.isSidebarMedium = false;
      this.isSidebarNone = false;
      // return 'isSidebarLarge';
    }
    else if(ww <= 666 && ww > 420) {
      this.isSidebarLarge = false;
      this.isSidebarMedium = true;
      this.isSidebarNone = false;
      // return 'isSidebarMedium';
    }
    else if(ww <= 420) {
      this.isSidebarLarge = false;
      this.isSidebarMedium = true;
      this.isSidebarNone = false;
      // return 'isSidebarNone';
    }
  }

}
