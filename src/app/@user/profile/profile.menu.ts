import { MenuItem } from 'app/@core/models/menuItem.model';

export const MENU_ITEMS: MenuItem[] = [
  {
    title: 'Front',
    icon: 'earth',
    link: '/',
    home: true,
  },
  {
    title: 'Profile',
    icon: 'account',
    link: '/user/profile',
    home: true,
  },
  {
    title: 'Calendar',
    icon: 'calendar-account',
    link: '/user/profile/calendar',
    home: true,
  },
  {
    title: 'Resources',
    icon: 'music',
    link: '/user/profile/resources',
    home: true,
  },
  {
    title: 'Logout',
    icon: 'logout',
    link: '[logout]',
    home: true,
  },
];
