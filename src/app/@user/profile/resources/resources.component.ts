import { Component, OnInit } from '@angular/core';
import { ProfileLinksService } from 'app/@core/services/_index';
import { ProfileLink } from 'app/@core/models/_index';

@Component({
  selector: 'resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.scss']
})
export class ResourcesComponent implements OnInit {
  pfls;

  constructor(private pflService: ProfileLinksService) { }

  ngOnInit() {
  }

}
