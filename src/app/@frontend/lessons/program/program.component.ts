import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router}  from '@angular/router';
import { SignupService } from 'app/@core/services/signup.service';
import { Signup } from 'app/@core/models/signup.model';
import { AlertService } from 'app/@core/services/alert.service';
import { SeoService } from 'app/@core/services/_index';

@Component({
  selector: 'program',
  templateUrl: './program.component.html',
  styleUrls: ['./program.component.scss']
})
export class ProgramComponent implements OnInit {
  signup: {
    'key': string,
    'email': string,
    'message': string,
    'name': string,
    'phone': string,
    'isRead': boolean,
    'createdAt': Date,
    'updatedAt': Date
  }

  constructor(private signupService: SignupService
    , private alertService: AlertService
    , private router: Router
    , private seoService: SeoService) {
    this.signup = {
      'key': '',
      'email': '',
      'message': '',
      'name': '',
      'phone': '',
      'isRead': false,
      'createdAt': new Date(),
      'updatedAt': new Date()
    }
  }

  ngOnInit() {
    this.seoService.generateTags({
      title: 'Program',
      description: 'MonaRose Studios is the best place to figure out your musical journey. With programs tailored to your specific needs we care about you personally!',
      image: '/assets/images/piano_4.jpg',
      slug: 'lessons/program'
    })
  }


  saveMessage(form: NgForm) {
    this.signupService.saveNewSignup(this.signup);
    form.reset();
    this.alertService.success("Thank you for signing up! We will contact you back shortly");
  }

}
