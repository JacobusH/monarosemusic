import { Component, OnInit } from '@angular/core';
import { TeacherService } from 'app/@core/services/_index';
import { Teacher } from 'app/@core/models/_index';
import { Observable } from 'rxjs';

@Component({
  selector: 'teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.scss']
})
export class TeachersComponent implements OnInit {
  teachers: Observable<Teacher[]>;

  constructor(private teacherService: TeacherService) { }

  ngOnInit() {
    // this.teachers = this.teacherService.teachersActive.valueChanges();
    this.teachers = this.teacherService.teachers.valueChanges();
  }

}
