import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router}  from '@angular/router';
import { SignupService } from 'app/@core/services/signup.service';
import { Signup } from 'app/@core/models/signup.model';
import { AlertService } from 'app/@core/services/alert.service';
import { SeoService } from 'app/@core/services/_index';
import { Subscription } from 'rxjs';

@Component({
  selector: 'lessons',
  templateUrl: './lessons.component.html',
  styleUrls: ['./lessons.component.scss']
})
export class LessonsComponent implements OnInit, OnDestroy {
  r$: Subscription;
  onSubpage: boolean = false;
  signup: {
    'key': string,
    'email': string,
    'message': string,
    'name': string,
    'phone': string,
    'isRead': boolean,
    'createdAt': Date,
    'updatedAt': Date
  }

  constructor(private signupService: SignupService
    , private alertService: AlertService
    , private router: Router
    , private seoService: SeoService) {
    this.signup = {
      'key': '',
      'email': '',
      'message': '',
      'name': '',
      'phone': '',
      'isRead': false,
      'createdAt': new Date(),
      'updatedAt': new Date()
    }
  }

  ngOnInit() {
    this.getOnSub();
    this.r$ = this.router.events.subscribe(x => {
      this.getOnSub();
    })
    this.seoService.generateTags({
      title: 'Lessons',
      description: 'MonaRose Studios is the best studio for piano lessons in Laguna Niguel, Laguna Hills, and all of South Orange County (OC). Our lessons help you grow as a musician and a person!',
      image: '/assets/images/piano_4.jpg',
      slug: 'lessons'
    })
  }

  ngOnDestroy() {
    this.r$.unsubscribe();
  }

  getOnSub() {
    let lIdx = window.location.pathname.indexOf('lessons');
    if(lIdx != -1) {
      if(window.location.pathname.length > 'lessons'.length + lIdx) { // on lessons subpage
        this.onSubpage = true;
      }
    }
  }

  saveMessage(form: NgForm) {
    this.signupService.saveNewSignup(this.signup);
    form.reset();
    this.alertService.success("Thank you for signing up! We will contact you back shortly");
  }

}
