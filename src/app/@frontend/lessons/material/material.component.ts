import { Component, OnInit } from '@angular/core';
import { SeoService } from 'app/@core/services/_index';

@Component({
  selector: 'material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.scss']
})
export class MaterialComponent implements OnInit {

  constructor(private seoService: SeoService) { }

  ngOnInit() {
    this.seoService.generateTags({
      title: 'Material & Music',
      description: 'MonaRose Studios has new and exciting music from a variety of disciplines. From electronic to classical, we can teach everything!',
      image: '/assets/images/piano_4.jpg',
      slug: 'lessons/material'
    })
  }

}
