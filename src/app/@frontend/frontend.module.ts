import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CoreModule } from 'app/@core/core.module';
import { FormsModule } from '@angular/forms';
import { FrontendRoutingModule } from './frontend-routing.module';
// import { NgScrollbarModule } from 'ngx-scrollbar';

import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { FooterComponent } from './footer/footer.component';
import { FrontendComponent } from './frontend.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LessonsComponent } from './lessons/lessons.component';
import { VideosComponent } from './music/videos/videos.component';
import { HeaderHomeComponent } from './header-home/header-home.component';
import { FooterHomeComponent } from './footer-home/footer-home.component';
import { ReferencesComponent } from './references/references.component';
import { ProgramComponent } from './lessons/program/program.component';
import { TeachersComponent } from './lessons/teachers/teachers.component';
import { MaterialComponent } from './lessons/material/material.component';
import { MusicComponent } from './music/music.component';
import { TutorialsComponent } from './music/tutorials/tutorials.component';

const COMPONENTS = [
  AboutComponent,
  ContactComponent,
  FooterComponent,
  FrontendComponent,
  HeaderComponent,
  HomeComponent,
  LessonsComponent,
  ReferencesComponent,
  VideosComponent,
  HeaderHomeComponent,
  FooterHomeComponent,
  ProgramComponent,
  TeachersComponent,
  MaterialComponent,
  MusicComponent,
  TutorialsComponent
];

@NgModule({
  declarations: [
    ... COMPONENTS
  ],
  imports: [
    FrontendRoutingModule
    , CommonModule
    , CoreModule
    , FormsModule
    , RouterModule
    // , NgScrollbarModule
  ]
})
export class FrontendModule { }
