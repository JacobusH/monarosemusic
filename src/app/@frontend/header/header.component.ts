import { Component, OnDestroy, OnInit, HostListener, Version } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from 'app/@core/services/auth.service';
import { first, tap } from 'rxjs/operators';
import { User } from 'app/@core/models/user.model';
import { UserService } from 'app/@core/services/user.service';
import { slideUpDownAnimation, apparateAnimation, inOutAnimation, listAnimation } from 'app/@core/animations/_index';
import { VersionService } from 'app/@core/services/_index';
import { MENU_ITEMS } from '../header.menu';
import { Subscription } from 'rxjs';

@Component({
  selector: 'header-front',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
  animations: [ slideUpDownAnimation, inOutAnimation, listAnimation ]
})
export class HeaderComponent implements OnInit, OnDestroy {
  as$: Subscription;
  vs$: Subscription;
  isLoggedIn: boolean = false;
  isAdmin: boolean = false;
  user: User;
  windowWidth: number;
  hamburgerDown: boolean;
  appVersion;
  menuItems;
  isLessonsSubShow: boolean = false;
  isMusicSubShow: boolean = false;

  constructor(
    private authService: AuthService
    , private afAuth: AngularFireAuth
    , private userService: UserService
    , private versionService: VersionService
    , private router: Router
    ) {

  }

  ngOnInit() {
    this.menuItems = MENU_ITEMS;
    console.log(this.menuItems)
    this.as$ = this.authService.user$.subscribe(x => {
      this.user = x;
      if(x && x.roles.admin) { // add admin option
        let found = false;
        for(var i = 0; i < this.menuItems.length; i++) {
          if(this.menuItems[i].title === 'Admin') {
            found = true;
          }
        }
        if(!found) {
          this.menuItems.push({title: 'Admin', link:'admin'});
        }
      }
    })
    this.vs$ = this.versionService.getAppVersion().subscribe(x => {
      this.appVersion = x;
    })
    this.windowWidth = window.innerWidth;
  }

  ngOnDestroy() {
    this.as$.unsubscribe();
    this.vs$.unsubscribe();
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    this.windowWidth = event.target.innerWidth;
    // console.log(event.target.innerWidth);
  }

  toggleHamburger() {
    this.hamburgerDown = !this.hamburgerDown;
    if(!this.hamburgerDown) {
      this.isLessonsSubShow = false;
      this.isMusicSubShow = false;
    }
  }

  mouseEnter(title: string, link?: string) {
    // console.log("title", title)
    if(title.toLocaleLowerCase() === 'lessons') {
      this.isLessonsSubShow = !this.isLessonsSubShow;
      this.isMusicSubShow = false;
    }
    else if(title.toLocaleLowerCase() === 'music') {
      this.isMusicSubShow = !this.isMusicSubShow;
      this.isLessonsSubShow = false;
    }
    else {
      this.isMusicSubShow = false;
      this.isLessonsSubShow = false;
      if(link) {
        this.router.navigate(['/'+link]);
        this.mouseLeave();
      }
    }
  }

  mouseLeave() {
    this.isLessonsSubShow = false;
    this.isMusicSubShow = false;
    this.hamburgerDown = false;
  }

  smallMouseEnter(title) {
    console.log('small', title)
  }

}
