import { Component, OnInit, HostListener } from '@angular/core';
import { Reference } from 'app/@core/models/reference.model';
import { ReferencesService } from 'app/@core/services/references.service';
import { SeoService } from 'app/@core/services/_index';

@Component({
  selector: 'references',
  templateUrl: './references.component.html',
  styleUrls: ['./references.component.scss']
})
export class ReferencesComponent implements OnInit {
  refs;
  windowWidth;
  curWidth;

  constructor(private referenceService: ReferencesService
    , private seoService: SeoService) {

  }

  ngOnInit() {
    this.refs = this.referenceService.references.valueChanges();
    this.windowWidth = window.innerWidth;
    this.getWidth();
    this.seoService.generateTags({
      title: 'About Us',
      description: 'MonaRose Studios has amazing piano lessons in Laguna Hills and Niguel!',
      image: '/assets/images/piano_4.jpg',
      slug: 'references'
    })
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    this.windowWidth = event.target.innerWidth;
    this.getWidth();
    // console.log(event.target.innerWidth);
  }

  getWidth() {
    if(this.windowWidth < 768) {
      this.curWidth = '100';
    }
    else if(this.windowWidth >= 768) {
      this.curWidth = '40';
    }
    // else {
    //   this.curWidth = '33';
    // }
  }

}
