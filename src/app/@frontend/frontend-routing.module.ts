import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { FrontendComponent } from './frontend.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { LessonsComponent } from './lessons/lessons.component';
import { ReferencesComponent } from './references/references.component';
import { VideosComponent } from './music/videos/videos.component';
import { ProgramComponent } from './lessons/program/program.component';
import { TeachersComponent } from './lessons/teachers/teachers.component';
import { MaterialComponent } from './lessons/material/material.component';
import { MusicComponent } from './music/music.component';
import { TutorialsComponent } from './music/tutorials/tutorials.component';

const routes: Routes = [
    { path: '', component: FrontendComponent,
      children: [
        { path: '', component: HomeComponent },
        { path: 'about', component: AboutComponent },
        { path: 'contact', component: ContactComponent },
        { path: 'home', component: HomeComponent },
        { path: 'lessons', component: LessonsComponent, children: [
          { path: 'program', component: ProgramComponent },
          { path: 'materials', component: MaterialComponent },
          { path: 'references', component: ReferencesComponent },
          { path: 'teachers', component: TeachersComponent },
        ]},
        { path: 'music', component: MusicComponent, children: [
          { path: 'tutorials', component: TutorialsComponent },
          { path: 'videos', component: VideosComponent },
        ]},
      ]
    },
    { path: '',redirectTo: '', pathMatch: 'full' },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FrontendRoutingModule {
}
