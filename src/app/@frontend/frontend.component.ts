import { Component, OnInit, OnDestroy } from '@angular/core';
import { VersionService } from 'app/@core/services/_index';
import { ActivatedRoute, Router}  from '@angular/router';
import { Subscription } from 'rxjs';


@Component({
  selector: 'frontend',
  templateUrl: './frontend.component.html',
  styleUrls: ['./frontend.component.scss']
})
export class FrontendComponent implements OnInit, OnDestroy {
  r$: Subscription;
  vs$: Subscription;
  appVersion;
  isHome = false;

  constructor(
    private versionService: VersionService,
    private actRoute: ActivatedRoute
    , private router: Router
    ) {
    // let adminIdx = window.location.pathname.indexOf('admin');
    // if(adminIdx != -1) {
    //   this.title = window.location.pathname.substring(adminIdx, window.location.pathname.length)
    // }
    this.r$ = this.router.events.subscribe(x => {
      this.isHome = false;
      let isOne = window.location.pathname.length == 1;
      if(isOne) { // homme page with no fragments
        this.isHome = true;
      }
      else {
        let path = window.location.pathname.substring(window.location.pathname.indexOf('/'), window.location.pathname.length).toLocaleLowerCase();
        if(path.indexOf('home') != -1) {
          this.isHome = true;
        }
      }

    })
  }

  ngOnInit() {
    this.vs$ = this.versionService.getAppVersion().subscribe(x => {
      this.appVersion = x;
    })
  }

  ngOnDestroy() {
    this.r$.unsubscribe();
    this.vs$.unsubscribe();
  }

}
