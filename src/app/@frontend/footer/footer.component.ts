import { Component, HostListener, OnInit} from '@angular/core';

@Component({
  selector: 'footer-front',
  styleUrls: ['./footer.component.scss'],
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {
  windowWidth;

  ngOnInit() {
    this.windowWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    this.windowWidth = event.target.innerWidth;
    // console.log(event.target.innerWidth);
  }
}
