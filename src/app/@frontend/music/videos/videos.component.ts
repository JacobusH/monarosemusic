import { Component, OnInit, ViewEncapsulation, OnChanges, Input, OnDestroy } from '@angular/core';
import { VideoItem, Upload } from 'app/@core/models/_index';
import { VideoItemService } from 'app/@core/services/_index';
import { Observable } from 'rxjs/Observable';
import { SeoService } from 'app/@core/services/_index';
import { Subscription } from 'rxjs';

@Component({
  selector: 'videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit, OnDestroy {
  @Input() filterBy?: string = 'all';
  va$: Subscription;
  videosActive: Observable<VideoItem[]>;
  filterOptions: Array<string> = [];
  filterMetadata: any;

  currentPage: number = 0;
  totalPages: number = 1;
  pageSize: number = 10;

  constructor(private videoService: VideoItemService
    , private seoService: SeoService) {
    // this.videosActive = this.videoService.videoItemsActive.valueChanges();
  }

  ngOnInit() {
    this.videosActive = this.videoService.videoItemsActive.valueChanges();
    // this.videosActive = this.videoService.getRange(0, 10).valueChanges();

    this.va$ = this.videosActive.subscribe(x => {

      for(var i = 0; i < x.length; i++) {
        let splits = x[i].categories.split(',');
        for(var j = 0; j < splits.length; j++) {
          var element = splits[j].replace(/\s/g, '');
          if(this.filterOptions.indexOf(element) === -1) {
            this.filterOptions.push(element);
          }
        }
      }

    })
    this.seoService.generateTags({
      title: 'Music Videos',
      description: 'MonaRose Studios has new music videos of cutting edge songs by the best in the business!',
      image: '/assets/images/piano_4.jpg',
      slug: 'music/videos'
    })
  }

  ngOnChanges() {

  }

  ngOnDestroy() {
    this.va$.unsubscribe();
  }

  filterClicked(filterApplied: string) {
    this.filterBy = filterApplied;
  }


}
