import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router}  from '@angular/router';
import { SeoService } from 'app/@core/services/_index';
import { Subscription } from 'rxjs';

@Component({
  selector: 'music',
  templateUrl: './music.component.html',
  styleUrls: ['./music.component.scss']
})
export class MusicComponent implements OnInit, OnDestroy {
  r$: Subscription;
  onSubpage: boolean = false;

  constructor(private router: Router
    , private seoService: SeoService) { }

  ngOnInit() {
    this.getOnSub();
    this.r$ = this.router.events.subscribe(x => {
      this.getOnSub();
    })
    this.seoService.generateTags({
      title: 'Music',
      description: 'MonaRose Studios has all the newest piano music for every variety of taste! Check us out now and see for yourself!',
      image: '/assets/images/piano_4.jpg',
      slug: 'music'
    })
  }

  ngOnDestroy() {
    this.r$.unsubscribe();
  }

  getOnSub() {
    let lIdx = window.location.pathname.indexOf('music');
    if(lIdx != -1) {
      if(window.location.pathname.length > 'music'.length + lIdx) { // on lessons subpage
        this.onSubpage = true;
      }
    }
  }

}
