import { Component, OnInit } from '@angular/core';
import { SeoService } from 'app/@core/services/_index';

@Component({
  selector: 'tutorials',
  templateUrl: './tutorials.component.html',
  styleUrls: ['./tutorials.component.scss']
})
export class TutorialsComponent implements OnInit {

  constructor(private seoService: SeoService) { }

  ngOnInit() {
    this.seoService.generateTags({
      title: 'Music Tutorials',
      description: 'MonaRose Studios in Laguna Hills offers video tutorials for all students! Learn at your own pace using modern tools!',
      image: '/assets/images/piano_4.jpg',
      slug: 'music/tutorials'
    })
  }

}
