import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { VersionService } from 'app/@core/services/_index';
import { SeoService } from 'app/@core/services/_index';
import { Subscription } from 'rxjs';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  // @ViewChild('videoPlayer', null) videoplayer: ElementRef;
  vs$: Subscription;
  appVersion;
  windowWidth;

  constructor(private versionService: VersionService
    , private seoService: SeoService) { }

  ngOnInit() {
    this.vs$ = this.versionService.getAppVersion().subscribe(x => {
      this.appVersion = x;
    })
    this.windowWidth = window.innerWidth;

    this.seoService.generateTags({
      title: 'MonaRose Studios',
      description: 'MonaRose Studios is the premier studio for piano lessons in Laguna Niguel, Laguna Hills, and all of South Orange County (OC). Check out our site and reach out for lessons today!',
      image: '/assets/images/piano_4.jpg',
      slug: 'about'
    })
  }

  ngOnDestroy() {
    this.vs$.unsubscribe();
  }

}
