import { Component, OnInit } from '@angular/core';
import { SeoService } from 'app/@core/services/_index';

@Component({
  selector: 'about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(private seoService: SeoService) { }

  ngOnInit() {
    this.seoService.generateTags({
      title: 'About Us',
      description: 'MonaRose Studios is the premier studio for piano lessons in Laguna Niguel, Laguna Hills, and all of South Orange County (OC). Read more about us today!',
      image: '/assets/images/piano_4.jpg',
      slug: 'about'
    })
  }

}
