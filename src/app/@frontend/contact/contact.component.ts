import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationStart }  from '@angular/router';
import { NgForm } from '@angular/forms';
import { ContactMsg } from 'app/@core/models/contact.model';
import { ContactService } from 'app/@core/services/contact.service';
import { SeoService } from 'app/@core/services/_index';

@Component({
  selector: 'contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contactMsg: {
    'key': string,
    'email': string,
    'message': string,
    'name': string,
    'phone': string,
    'isRead': boolean,
    'createdAt': Date,
    'updatedAt': Date
  }

  constructor(private contactService: ContactService
    , private seoService: SeoService) {
    this.contactMsg = {
      'key': '',
      'email': '',
      'message': '',
      'name': '',
      'phone': '',
      'isRead': false,
      'createdAt': new Date(),
      'updatedAt': new Date()
    }
  }

  ngOnInit() {
    this.seoService.generateTags({
      title: 'Contact Us',
      description: 'MonaRose Studios is the premier studio for piano lessons in Laguna Niguel, Laguna Hills, and all of South Orange County (OC). Contact us for great prices and rates!',
      image: '/assets/images/piano_4.jpg',
      slug: 'contact'
    })
  }

  saveMessage(form: NgForm) {
    this.contactService.saveNewContact(this.contactMsg);
    form.reset();
  }

}
