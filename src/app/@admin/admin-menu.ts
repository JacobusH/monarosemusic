import { MenuItem } from 'app/@core/models/menuItem.model';

export const MENU_ITEMS: MenuItem[] = [
  {
    title: 'Front',
    icon: 'earth',
    link: '/',
    home: true,
  },
  {
    title: 'Dashboard',
    icon: 'home',
    link: '/admin/dashboard',
    home: true,
  },
  {
    title: 'Notes',
    icon: 'lead-pencil',
    link: '/admin/notes',
    home: true,
  },
  {
    title: 'Page Pictures',
    icon: 'image-multiple',
    link: '/admin/page-pictures',
    home: true,
  },
  {
    title: 'Profile Links',
    icon: 'link-variant',
    link: '/admin/profile-links',
    home: true,
  },
  {
    title: 'References',
    icon: 'glasses',
    link: '/admin/references',
    home: true,
  },
  {
    title: 'Teachers',
    icon: 'school',
    link: '/admin/teachers',
    home: true,
  },
  {
    title: 'Text Sections',
    icon: 'file-document-outline',
    link: '/admin/text-sections',
    home: true,
  },
  {
    title: 'User Manager',
    icon: 'account-supervisor',
    link: '/admin/user-manager',
    home: true,
  },
  {
    title: 'Video',
    icon: 'video-outline',
    link: '/admin/videos',
    home: true,
  },
  // {
  //   title: 'Frontend Manager',
  //   icon: 'image-outline',
  //   link: '/admin/frontend-manager',
  //   home: true,
  // },
  // {
  //   title: 'FEATURES',
  //   group: true,
  // },
  // {
  //   title: 'Pages',
  //   icon: 'folder-outline',
  //   children: [
  //     {
  //       title: 'Home',
  //       link: '/admin/home',
  //     },
  //     {
  //       title: 'About',
  //       link: '/admin/about',
  //     },
  //     {
  //       title: 'Contact',
  //       link: '/admin/contact',
  //     },
  //     {
  //       title: 'Lessons',
  //       link: '/admin/lessons',
  //     },
  //     {
  //       title: 'Music',
  //       link: '/admin/music',
  //     },
  //     {
  //       title: 'Videos',
  //       link: '/admin/videos',
  //     },
  //     {
  //       title: 'Profile',
  //       link: '/admin/profile',
  //     },
  //     {
  //       title: 'Materials',
  //       children: [
  //         {
  //           title: 'Materials',
  //           link: '/admin/materials',
  //         },
  //         {
  //           title: 'Books',
  //           link: '/admin/materials/books',
  //         },
  //         {
  //           title: 'Keyboards',
  //           link: '/admin/materials/keyboards',
  //         },
  //         {
  //           title: 'Sheet Music',
  //           link: '/admin/materials/sheet-music',
  //         },
  //         {
  //           title: 'Solo Pieces',
  //           link: '/admin/materials/solo-pieces',
  //         },
  //       ],
  //     },
  //   ],
  // },
];
