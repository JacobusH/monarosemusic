import { Component } from '@angular/core';

@Component({
  selector: 'ngx-one-column-layout',
  styleUrls: ['./one-column.layout.scss'],
  template: `
  <ng-content select="router-outlet"></ng-content>
  TEST
  `,
})
export class OneColumnLayoutComponent {}
