import { AdminRoutingModule } from './admin-routing.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CoreModule } from 'app/@core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';

import { AdminComponent } from './admin.component';
import { FooterAdminComponent, HeaderAdminComponent } from 'app/@core/components';
import { OneColumnLayoutComponent } from './layouts';
import { AdminProfileComponent } from './components/admin-profile/admin-profile.component';
import { AdminTextSectionsComponent } from './components/admin-text-sections/admin-text-sections.component';
import { AdminPagePicturesComponent } from './components/admin-page-pictures/admin-page-pictures.component';
import { FrontendManagerComponent } from './components/frontend-manager/frontend-manager.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AdminSuperComponent } from './components/admin-super/admin-super.component';
import { AdminReferencesComponent } from './components/admin-references/admin-references.component';
import { AdminProfileLinksComponent } from './components/admin-profile-links/admin-profile-links.component';
import { AdminVideosComponent  } from './components/admin-videos/admin-videos.component';
import { AdminUserManagerComponent } from './components/admin-user-manager/admin-user-manager.component';
import { AdminNotesComponent } from './components/admin-notes/admin-notes.component';
import { AdminTeachersComponent } from './components/admin-teachers/admin-teachers.component';

export const COMPONENTS = [
  AdminComponent
  , AdminNotesComponent
  , AdminPagePicturesComponent
  , AdminProfileComponent
  , AdminProfileLinksComponent
  , AdminReferencesComponent
  , AdminSuperComponent
  , AdminTextSectionsComponent
  , AdminUserManagerComponent
  , AdminVideosComponent
  , DashboardComponent
  , FooterAdminComponent
  , FrontendManagerComponent
  , HeaderAdminComponent
  , OneColumnLayoutComponent
];


@NgModule({
  declarations: [
    ... COMPONENTS,
    AdminTeachersComponent
  ],
  imports: [
    AdminRoutingModule,
    CommonModule,
    CoreModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule
  ]
})
export class AdminModule {
}
