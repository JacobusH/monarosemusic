import { Component, OnInit, OnDestroy } from '@angular/core';
import { TextSectionService, VersionService } from 'app/@core/services/_index';
import { TextSection } from 'app/@core/models/text-section.model';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'text-sections',
  templateUrl: './admin-text-sections.component.html',
  styleUrls: ['./admin-text-sections.component.scss']
})
export class AdminTextSectionsComponent implements OnInit, OnDestroy {
  textSections;
  model = this.tsService.createNew('', '');
  selectedItem: TextSection;
  currentPage: number = 0;
  totalPages: number = 1;
  pageSize: number = 10;
  isCreate: boolean;
  vs$: Subscription;

  constructor(private tsService: TextSectionService
    , private versionService: VersionService) { }

  ngOnInit() {
    this.textSections = this.tsService.getAll();
    this.vs$ = this.versionService.getIsCreate().subscribe(x => {
      this.isCreate = x;
      // console.log('subby', this.isCreate)
    })
  }

  ngOnDestroy() {
    this.vs$.unsubscribe();
  }

  // saveNewTS(tsText: string, tsShortID: string) {
  //   this.tsService.saveNewTextSection({
  //     key: "tmp",
  //     shortID: tsShortID,
  //     html: tsText
  //   })
  // }

  saveItem(form: NgForm) {
    // editing item
    if(this.selectedItem != null) {
      this.tsService.edit(this.selectedItem)
      form.reset();
    }
    else { // new item
      this.model.key = 'tmp';
      this.tsService.saveNewTextSection(this.model);
      this.model = this.tsService.createNew('', '');
      form.reset();
    }
  }

  setNewItem() {
    this.selectedItem = null;
    this.model = this.tsService.createNew('', '');
  }

  setSelectedItem(item: TextSection) {
    this.selectedItem = item;
    this.model = item;
  }

}
