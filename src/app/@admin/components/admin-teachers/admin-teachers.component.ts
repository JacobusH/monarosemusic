import { Component, OnInit, ViewChild } from '@angular/core';
import { TeacherService, UploadService } from 'app/@core/services/_index';
import { Teacher, Upload } from 'app/@core/models/_index';
import { Observable } from 'rxjs';


@Component({
  selector: 'admin-teachers',
  templateUrl: './admin-teachers.component.html',
  styleUrls: ['./admin-teachers.component.scss']
})
export class AdminTeachersComponent implements OnInit {
  @ViewChild('fileUpload', {static: false}) fileUploadVar: any;
  @ViewChild('imgItemSelected', {static: false}) imgItemSelected: any;
  modelTeacher = this.teacherService.createNew();
  teachers: Observable<Teacher[]>;
  teachersActive: Observable<Teacher[]>;
  selectedTeacher: Teacher;
  selectedFiles: FileList;
  selectedPicture: string;
  currentUpload: Upload;
  firebaseUrl: string = 'gs://monarosestudios-db5fe.appspot.com/';
  STORAGE_TEACHERS: string = 'teachers';
  otalPages: number = 1;
  pageSize: number = 10;

  constructor(
    private teacherService: TeacherService
    ,  private upsvc: UploadService) {

  }

  ngOnInit() {
    this.teachers = this.teacherService.teachers.valueChanges();
    this.teachersActive = this.teacherService.teachersActive.valueChanges();
  }

  saveItem() {
    // uploading new pic
    if(this.selectedFiles != null && this.selectedTeacher == null) {
      this.uploadSingleItem();
      this.modelTeacher = this.teacherService.createNew();
      this.fileUploadVar.nativeElement.value = "";
      // form.reset();
    }
    // editing pic
    else if(this.selectedTeacher != null) {
      if(this.selectedFiles != null) {
        this.uploadSingleItem();
      }
      this.teacherService.edit(this.selectedTeacher);
      this.fileUploadVar.nativeElement.value = "";
      // form.reset();
    }
    this.setNewItem();
  }

  detectFiles(event) {
    this.selectedFiles = event.target.files;
  }

  uploadSingleItem() {
    let file = this.selectedFiles.item(0);
    this.currentUpload = new Upload(file);

    this.upsvc.pushUpload(this.currentUpload, this.STORAGE_TEACHERS, this.modelTeacher);
  }

  setNewItem() {
    this.selectedTeacher = null;
    this.modelTeacher = this.teacherService.createNew();
    this.fileUploadVar.nativeElement.src = "";
  }

  setSelectedItem(item: Teacher) {
    this.selectedTeacher = item;
    this.modelTeacher = item;
    // and save new one as active
    item.isActive = true;
    this.teacherService.edit(item);
  }

  deleteItem() {
    this.teacherService.delete(this.selectedTeacher);
    this.setNewItem();
  }

}
