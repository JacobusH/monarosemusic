import { Component, OnInit, OnDestroy } from '@angular/core';
import { VersionService } from 'app/@core/services/_index';
import { Subscription } from 'rxjs';

@Component({
  selector: 'admin-super',
  templateUrl: './admin-super.component.html',
  styleUrls: ['./admin-super.component.scss']
})
export class AdminSuperComponent implements OnInit, OnDestroy {
  curAppVersion:  number;
  isCreate:  boolean;
  c$: Subscription;
  v$: Subscription;

  constructor(private versionService: VersionService) {

  }

  ngOnInit() {
    this.v$ = this.versionService.getAppVersion().subscribe(x => {
      this.curAppVersion = x;
    })
    this.c$ = this.versionService.getIsCreate().subscribe(x => {
      this.isCreate = x;
    })
  }

  changeVersion(val) {
    this.versionService.setAppVersion(val);
  }

  changeIsCreate(val) {
    this.versionService.setIsCreate(val);
  }

  ngOnDestroy() {
    this.c$.unsubscribe();
    this.v$.unsubscribe();
  }

}
