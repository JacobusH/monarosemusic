import { Component, OnDestroy, OnInit, Input } from '@angular/core';
// import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';
import { SidebarService } from 'app/@core/services/sidebar.service';

import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'header-admin',
  templateUrl: './header-admin.component.html',
  styleUrls: ['./header-admin.component.scss']
})
export class HeaderAdminComponent implements OnInit {
  @Input() title: string;

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;


  constructor(private sidebarService: SidebarService) {
  }

  ngOnInit() {

  }

  ngOnDestroy() {

  }


  toggleSidebar(): boolean {
    this.sidebarService.toggle();
    return false;
  }

  navigateHome() {
    return false;
  }

}
