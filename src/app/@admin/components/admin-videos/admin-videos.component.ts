import { Component, OnInit, ViewEncapsulation, ViewChild, Input, HostListener, OnDestroy } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { VideoItem, Upload } from 'app/@core/models/_index';
import { VideoItemService } from 'app/@core/services/videoItem.service';
import { NgForm} from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';
import { Subscription } from 'rxjs';

@Component({
  selector: 'admin-videos',
  templateUrl: './admin-videos.component.html',
  styleUrls: ['./admin-videos.component.scss']
})
export class AdminVideosComponent implements OnInit, OnDestroy {
  items$: Subscription;
  model = this.videoService.createNew();
  selectedItem: VideoItem;
  items: Observable<VideoItem[]>;
  filterMetadata: any;
  windowWidth;

  @Input() filterBy?: string = 'all';
  filterOptions: Array<string> = [];

  currentPage: number = 0;
  totalPages: number = 1;
  pageSize: number = 10;


  constructor(private videoService: VideoItemService) {

  }

  ngOnInit() {
    this.items = this.videoService.videoItems.valueChanges();
    this.windowWidth = window.innerWidth;

    this.items$ = this.items.subscribe(x => {
      for(var i = 0; i < x.length; i++) {
        let splits = x[i].categories.split(',');
        for(var j = 0; j < splits.length; j++) {
          var element = splits[j].replace(/\s/g, '');
          if(this.filterOptions.indexOf(element) === -1) {
            this.filterOptions.push(element);
          }
        }
      }
    })
  }

  ngOnDestroy() {
    this.items$.unsubscribe();
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    this.windowWidth = event.target.innerWidth;
    // console.log(event.target.innerWidth);
  }


  filterClicked(filterApplied: string) {
    this.filterBy = filterApplied;
 }

  saveItem(form: NgForm) {
    // editing item
    if(this.selectedItem != null) {
      console.log(this.selectedItem)
      this.videoService.edit(this.selectedItem)
      form.reset();
    }
    else { // new item
      this.videoService.save(this.model);
      this.model = this.videoService.createNew();
      form.reset();
    }
  }

  setNewItem() {
    this.selectedItem = null;
    this.model = this.videoService.createNew();
  }

  setSelectedItem(item: VideoItem) {
    this.selectedItem = item;
    this.model = item;
  }

  deleteItem(form: NgForm) {
    form.reset();
    this.videoService.delete(this.selectedItem);
  }

  resetForm() {
    console.log('in reset')
    this.selectedItem = null;
    this.model.isActive = true;
  }

}
