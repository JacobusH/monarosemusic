import { Component, OnInit } from '@angular/core';
import { NgForm} from '@angular/forms';
import { ProfileLink } from 'app/@core/models/_index';
import { ProfileLinksService } from 'app/@core/services/_index';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'admin-profile-links',
  templateUrl: './admin-profile-links.component.html',
  styleUrls: ['./admin-profile-links.component.scss']
})
export class AdminProfileLinksComponent implements OnInit {
  model;
  selectedItem: ProfileLink;
  items: Observable<ProfileLink[]>;

  constructor(private pflService: ProfileLinksService) { }

  ngOnInit() {
    this.model = this.pflService.createNew();
    this.items = this.pflService.profileLinks.valueChanges();
  }

  saveItem(form: NgForm) {
    // editing item
    if(this.selectedItem != null) {
      // console.log(this.selectedItem)
      this.pflService.edit(this.selectedItem)
      form.reset();
    }
    else { // new item
      this.model.link = this.model.link.match(/src=\"(.*)\"/)[1];
      this.pflService.save(this.model);
      this.model = this.pflService.createNew();
      form.reset();
    }
  }

  setNewItem() {
    this.selectedItem = null;
    this.model = this.pflService.createNew();
  }

  setSelectedItem(item: ProfileLink) {
    console.log('item', item)
    this.selectedItem = item;
    this.model = item;
  }

  deleteItem(form: NgForm) {
    form.reset();
    this.resetForm(form);
    this.pflService.delete(this.selectedItem);
  }

  resetForm(form: NgForm) {
    // console.log('in reset')
    this.setNewItem();

  }

}
