import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminProfileLinksComponent } from './admin-profile-links.component';

describe('AdminProfileLinksComponent', () => {
  let component: AdminProfileLinksComponent;
  let fixture: ComponentFixture<AdminProfileLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminProfileLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProfileLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
