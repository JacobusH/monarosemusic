import { Component, OnInit } from '@angular/core';
import { TextSectionService } from 'app/@core/services/_index';
import { TextSection } from 'app/@core/models/_index';
import { Observable } from 'rxjs';

@Component({
  selector: 'frontend-manager',
  templateUrl: './frontend-manager.component.html',
  styleUrls: ['./frontend-manager.component.scss']
})
export class FrontendManagerComponent implements OnInit {
  textSections: Observable<TextSection>;
  tsSelected: TextSection;

  constructor() { }

  ngOnInit() {

  }

}
