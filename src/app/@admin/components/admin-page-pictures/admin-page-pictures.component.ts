import { Component, OnInit, OnDestroy } from '@angular/core';
import { PagePictureService, VersionService } from 'app/@core/services/_index';
import { PagePicture, PagePictureContainer } from 'app/@core/models/_index';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'admin-page-pictures',
  templateUrl: './admin-page-pictures.component.html',
  styleUrls: ['./admin-page-pictures.component.scss']
})
export class AdminPagePicturesComponent implements OnInit, OnDestroy {
  isCreate = false;
  vs$: Subscription

  constructor(private versionService: VersionService) { }

  ngOnInit() {
    this.vs$ = this.versionService.getIsCreate().subscribe(x => {
      this.isCreate = x;
      // console.log('subby', this.isCreate)
    })
  }

  ngOnDestroy() {
    this.vs$.unsubscribe();
  }



}
