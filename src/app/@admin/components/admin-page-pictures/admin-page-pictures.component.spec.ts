import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPagePicturesComponent } from './admin-page-pictures.component';

describe('AdminPagePicturesComponent', () => {
  let component: AdminPagePicturesComponent;
  let fixture: ComponentFixture<AdminPagePicturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPagePicturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPagePicturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
