import { Component, OnInit } from '@angular/core';
import { User } from 'app/@core/models/_index';
import { UserService } from 'app/@core/services/_index';
import { Observable } from 'rxjs';

@Component({
  selector: 'admin-user-manager',
  templateUrl: './admin-user-manager.component.html',
  styleUrls: ['./admin-user-manager.component.scss']
})
export class AdminUserManagerComponent implements OnInit {
  users$: Observable<User[]>;
  roles = ['Guest', 'Student', 'Teacher', 'Admin'];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.users$ = this.userService.users.valueChanges();
  }

  changePermission(user: User, which: string) {
    if(which.toLocaleLowerCase() == 'admin') {
      if(confirm("Are you sure you want to change admin permissions for " + user.fullName + "?")) {
        user.roles[which] = !user.roles[which];
        this.userService.edit(user);
      }
    }
    else if(which.toLocaleLowerCase() == 'guest' && user.roles['admin'] == true && user.roles['guest'] == false) {
      if(confirm("Are you sure you want to change " +user.fullName + " from an admin to a guest?")) {
        user.roles[which] = !user.roles[which];
        user = this.changeGuest(user, which);
        this.userService.edit(user);
      }
    }
    else {
      user.roles[which] = !user.roles[which];
      user = this.changeGuest(user, which);
      this.userService.edit(user);
    }
  }

  changeGuest(user: User, changeTo: string) {
    if(user.roles['guest'] == true && changeTo == 'guest') { // remove others
      for (let key in user.roles) {
        if(key != 'guest') {
          user.roles[key] = false;
        }
      }
    }
    return user;
  }

}
