import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { ContactService, SignupService } from 'app/@core/services/_index';
import { ContactMsg, Signup } from 'app/@core/models/_index';
import { slideUpDownAnimation, apparateAnimation, inOutAnimation, listAnimation, dashAnimation } from 'app/@core/animations/_index';
import { DatePipe } from '@angular/common';
import { VersionService } from 'app/@core/services/_index';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [ slideUpDownAnimation, apparateAnimation, inOutAnimation, listAnimation, dashAnimation ]
})
export class DashboardComponent implements OnInit {
  @ViewChild('contactHeader', {static: false}) contactHeader: any;
  @ViewChild('contactBig', {static: false}) contactBig: any;
  contactMsgs;
  signups;
  isCollapsedSignup = false;
  isCollapsedContact = false;
  windowWidth;
  isShowMoreInfo = false;
  isShowFullMsg = false;

  constructor(
    private contactService: ContactService
    , private signupService: SignupService) {

  }

  ngOnInit() {
    this.contactMsgs = this.contactService.contactMsgs.valueChanges();
    this.signups = this.signupService.signups.valueChanges();
    this.windowWidth = window.innerWidth;
  }

  toggle(which: string) {
    if(which === 'signup') { this.isCollapsedSignup = !this.isCollapsedSignup; }
    if(which === 'contact') { this.isCollapsedContact = !this.isCollapsedContact; }
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    this.windowWidth = event.target.innerWidth;
    // console.log(event.target.innerWidth);
  }

  showMoreInfo() {
    this.isShowMoreInfo = !this.isShowMoreInfo;
  }

  showFullMsg() {
    this.isShowFullMsg = !this.isShowFullMsg;
  }



}
