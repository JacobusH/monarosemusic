import { Component, OnInit } from '@angular/core';
import { NgForm} from '@angular/forms';
import { Reference } from 'app/@core/models/reference.model';
import { ReferencesService } from 'app/@core/services/_index';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'admin-references',
  templateUrl: './admin-references.component.html',
  styleUrls: ['./admin-references.component.scss']
})
export class AdminReferencesComponent implements OnInit {
  model = this.referenceService.createNew();
  selectedItem: Reference;
  items: Observable<Reference[]>;

  constructor(private referenceService: ReferencesService) { }

  ngOnInit() {
    this.items = this.referenceService.references.valueChanges();
  }

  saveItem(form: NgForm) {
    // editing item
    if(this.selectedItem != null) {
      // console.log(this.selectedItem)
      this.referenceService.edit(this.selectedItem)
      form.reset();
    }
    else { // new item
      this.referenceService.save(this.model);
      this.model = this.referenceService.createNew();
      form.reset();
    }
  }

  setNewItem() {
    this.selectedItem = null;
    this.model = this.referenceService.createNew();
  }

  setSelectedItem(item: Reference) {
    this.selectedItem = item;
    this.model = item;
  }

  deleteItem(form: NgForm) {
    form.reset();
    this.referenceService.delete(this.selectedItem);
  }

  resetForm() {
    // console.log('in reset')
    this.selectedItem = null;
  }

}
