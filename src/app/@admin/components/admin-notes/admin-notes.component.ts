import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Note } from 'app/@core/models/_index';
import { NoteService } from 'app/@core/services/_index';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'admin-notes',
  templateUrl: './admin-notes.component.html',
  styleUrls: ['./admin-notes.component.scss']
})
export class AdminNotesComponent implements OnInit, OnDestroy {
  notes$: Observable<Note[]>;
  newNote: string;
  filterBy: string = 'UnFinished';
  filterOptions: Array<string> = ['UnFinished', 'Finished', 'All'];

  constructor(private noteService: NoteService) { }

  ngOnInit() {
    this.notes$ = this.noteService.notes.valueChanges();
  }

  ngOnDestroy() {

  }

  reply(note: Note) {
    console.log('replying...');
  }

  getShown(note: Note) {
    return this.filterBy == 'All' || (this.filterBy == 'Finished' && note.isFinished) || (this.filterBy == 'UnFinished' && !note.isFinished);
  }

  getHidden(note: Note) {
    return !this.getShown(note);
  }

  saveNewNote(form: NgForm) {
    let toSave = this.noteService.createNew();
    toSave.message = this.newNote;
    this.noteService.saveNewNote(toSave);
    this.resetForm(form);
  }

  resetForm(form: NgForm) {
    form.reset();
  }

  changeIsRead(note: Note, state: boolean) {
    note.isRead = state;
    this.noteService.edit(note);
  }

  changeIsFinished(note: Note, state: boolean) {
    note.isFinished = state;
    this.noteService.edit(note);
  }

  filterClicked(filterApplied: string) {
    this.filterBy = filterApplied;
  }

}
