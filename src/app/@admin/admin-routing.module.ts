import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AdminTextSectionsComponent } from './components/admin-text-sections/admin-text-sections.component';
import { AdminComponent } from './admin.component';
import { AdminNotesComponent } from './components/admin-notes/admin-notes.component';
import { AdminPagePicturesComponent } from './components/admin-page-pictures/admin-page-pictures.component';
import { AdminProfileLinksComponent } from './components/admin-profile-links/admin-profile-links.component';
import { AdminReferencesComponent } from './components/admin-references/admin-references.component';
import { AdminTeachersComponent } from './components/admin-teachers/admin-teachers.component';
import { AdminSuperComponent } from './components/admin-super/admin-super.component';
import { AdminUserManagerComponent } from './components/admin-user-manager/admin-user-manager.component';
import { AdminVideosComponent } from './components/admin-videos/admin-videos.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

import { AdminGuard } from 'app/@core/guard/admin.guard';
import { SuperAdminGuard } from 'app/@core/guard/super-admin.guard';

const routes: Routes = [{
  path: '',
  component: AdminComponent,
  canActivate: [ AdminGuard ],
  children: [
    { path: 'dashboard', component: DashboardComponent, canActivate: [ AdminGuard ]},
    { path: 'notes', component: AdminNotesComponent, canActivate: [ AdminGuard ]},
    { path: 'page-pictures', component: AdminPagePicturesComponent, canActivate: [ AdminGuard ]},
    { path: 'profile-links', component: AdminProfileLinksComponent, canActivate: [ AdminGuard ]},
    { path: 'references', component: AdminReferencesComponent, canActivate: [ AdminGuard ]},
    { path: 'teachers', component: AdminTeachersComponent, canActivate: [ AdminGuard ]},
    { path: 'text-sections', component: AdminTextSectionsComponent, canActivate: [ AdminGuard ]},
    { path: 'user-manager', component: AdminUserManagerComponent, canActivate: [ AdminGuard ]},
    { path: 'videos', component: AdminVideosComponent, canActivate: [ AdminGuard ]},
    { path: 'super', component: AdminSuperComponent, canActivate: [ SuperAdminGuard ]},
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [
    RouterModule
  ],
})
export class AdminRoutingModule {
}
